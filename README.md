# Projet Location de voitures.
> Projet web permettant de louer un voiture ou de mettre sa voiture a louer. Ce projet a été fait avec Laravel 8 dans le cadre du cours de Web 2. 


## Table des matières
* [Info générale](#info-générale)
* [Technologies](#technologies)
* [Installation](#installation)
* [Fonctionnalités](#fonctionnalités)
* [Contact](#contact)

## Info générale
Ce projet a été réalisé dans le cadre du cours Web 2 pendant la session d'hiver 2020. 

Notre mandat était de faire faire la conception du projet, l'analyse et le code pour 
2 CRUDs par personne dans l'équipe dont un crud simple et un complexe ayant plusieurs 
relations. 

Les CRUDs pour cette version du projet sont les suivants : 

CRUDs simples : Marque et Entrepôt
CRUDs complexes : Modèle et Véhicule.

Les CRUDs 'Entrepot' et 'Modele' ont été réalisés par Andronik.
Les CRUDs 'Marque' et 'Vehicule' ont été réalisé par Ezequiel.

Le projet contient également un système de User fourni par Laravel. 
Pour accéder au CRUDs, un compte admin doit aller dans la section 'Réglages' du site.
Un compte qui n'est pas admin n'aura pas accès à la section 'Réglages' du site.


## Technologies
*  Laravel 8.12 (utilise PHP 7.4.11)

## Installation
Du au conditions particulières de la session, un guide d'installation n'est pas requis.
### Préalables
* Vous devez avoir PHP 7.4 ainsi qu'un serveur MySQL/MariaDB. Utliser XAMPP et la solution la plus simple.
* Vous devez avoir installé Laravel 8 : https://laravel.com/docs/8.x

### Téléchargement du projet
Clone HTTP a partir de gitlab en suivant le lien suivant : https://gitlab.com/Jogizy/projet_cn2

### Démarrage
Pour démmarer le projet en Laravel : 

```
git checkout master
```
Copiez le fichier .env.example dans .env et modifiez la ligne **DB_DATABASE=location_voitures_bd** et toute autre ligne pour adapter
le projet a votre environnement local.

Création de la base de données **location_voitures_bd** :

Entrez les commandes suivantes dans un terminal:

```
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
```

Pour démarrer le projet :

Entrez la commande suivante dans un terminal:

```
php artisan serve
```

Dans un navigateur, allez à l'adresse **localhost:8000**.

## Fonctionnalités
Fonctionnalités déjà réalisées:
*Page principale avec une barre de navigation contenant un bouton 'Login'
*Système de droit admin et user.
*Page 'Réglage' accessible seulement à un compte admin contenant des liens pour les CRUDs
*CRUDS fonctionnel : 'Entrepot', 'Modele' , 'Marque' et 'Vehicule'.
*Validation des entrées des CRUDs en utilisant des 'require'.
*Relations entre les tables des CRUDs fonctionnels.

## Contact
Créé par [Andronik et Ezequiel]
