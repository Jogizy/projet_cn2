<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location', function (Blueprint $table) {
            $table->id();
            $table->date('date_location');
            $table->date('date_retour');
            $table->unsignedBiginteger('users_id');
            $table->unsignedBiginteger('vehicule_id');
            $table->unsignedBiginteger('assurance_id');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('vehicule_id')->references('id')->on('vehicule')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('assurance_id')->references('id')->on('assurance')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
}
