<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicule', function (Blueprint $table) {
            $table->id();
            $table->string('vin')->unique();
            $table->string('plaque');
            $table->integer('annee');
            $table->string('couleur');
            $table->integer('km');
            $table->string('description');
            $table->date('date_disponibilite');
            $table->boolean('est_disponible');
            $table->unsignedBiginteger('entrepot_id');
            $table->unsignedBiginteger('modele_id');
            
            $table->foreign('entrepot_id')->references('id')->on('entrepot')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('modele_id')->references('id')->on('modele')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicule');
    }
}
