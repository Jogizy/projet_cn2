<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Assurance;
use App\Models\Modele;
use Illuminate\Support\Facades\DB;

class AssuranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assurance')->delete();

        $modele1 = Modele::where('nom', 'Civic')->first();
        $modele2 = Modele::where('nom', 'Odyssey')->first();
        $modele3 = Modele::where('nom', 'Tiguan')->first();
        $modele4 = Modele::where('nom', 'AMG')->first();

        $assurance = new Assurance();
        $assurance->type_assurance = "Base";
        $assurance->prix = 50;
        $assurance->modele_id = $modele1->id;
        $assurance->save();

        $assurance = new Assurance();
        $assurance->type_assurance = "Base";
        $assurance->prix = 50;
        $assurance->modele_id = $modele2->id;
        $assurance->save();

        $assurance = new Assurance();
        $assurance->type_assurance = "Plus";
        $assurance->prix = 70;
        $assurance->modele_id = $modele3->id;
        $assurance->save();

        $assurance = new Assurance();
        $assurance->type_assurance = "Premium";
        $assurance->prix = 150;
        $assurance->modele_id = $modele4->id;
        $assurance->save();
    }
}
