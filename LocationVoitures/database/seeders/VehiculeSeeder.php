<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Vehicule;
use App\Models\Entrepot;
use App\Models\Modele;
use Illuminate\Support\Facades\DB;

class VehiculeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicule')->delete();

        $entrepot1 = Entrepot::where('nom', '=', 'Entrepot 1')->first();
        $modele1 = Modele::where('nom','Civic')->first();
        $modele2 = Modele::where('nom','Odyssey')->first();
        $modele3 = Modele::where('nom','Tiguan')->first();
        $modele4 = Modele::where('nom','AMG')->first();

        $vehicule = new \App\Models\Vehicule();
        $vehicule->entrepot_id = $entrepot1->id;
        $vehicule->modele_id = $modele1->id;
        $vehicule->vin = 11111111;
        $vehicule->plaque = "plaque1";
        $vehicule->annee = 1993;
        $vehicule->couleur = "Rouge";
        $vehicule->km = 123456;
        $vehicule->description = "une belle Civic";
        $vehicule->date_disponibilite = '2020-11-24';
        $vehicule->est_disponible = True;
        $vehicule->save();

        $vehicule = new \App\Models\Vehicule();
        $vehicule->entrepot_id = $entrepot1->id;
        $vehicule->modele_id = $modele2->id;
        $vehicule->vin = 12231212;
        $vehicule->plaque = "plaque2";
        $vehicule->annee = 2007;
        $vehicule->couleur = "Vert";
        $vehicule->km = 123456;
        $vehicule->description = "une belle Odyssey";
        $vehicule->date_disponibilite = '2020-11-30';
        $vehicule->est_disponible = False;
        $vehicule->save();

        $vehicule = new \App\Models\Vehicule();
        $vehicule->entrepot_id = $entrepot1->id;
        $vehicule->modele_id = $modele3->id;
        $vehicule->vin = 35645242;
        $vehicule->plaque = "plaque3";
        $vehicule->annee = 2010;
        $vehicule->couleur = "Bleu";
        $vehicule->km = 123456;
        $vehicule->description = "une belle Tiguan";
        $vehicule->date_disponibilite = '2020-11-24';
        $vehicule->est_disponible = True;
        $vehicule->save();

        $vehicule = new \App\Models\Vehicule();
        $vehicule->entrepot_id = $entrepot1->id;
        $vehicule->modele_id = $modele4->id;
        $vehicule->vin = 786553213;
        $vehicule->plaque = "plaque4";
        $vehicule->annee = 2016;
        $vehicule->couleur = "Noir";
        $vehicule->km = 123456;
        $vehicule->description = "une tabarnouche de belle Benz";
        $vehicule->date_disponibilite = '2020-12-24';
        $vehicule->est_disponible = False;
        $vehicule->save();
    }
}
