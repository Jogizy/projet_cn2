<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PrixSaisonnier;
use App\Models\Vehicule;
use Illuminate\Support\Facades\DB;

class PrixSaisonnierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prixsaisonnier')->delete();

        $vehicule1 = Vehicule::where('plaque', 'plaque1')->first();
        $vehicule2 = Vehicule::where('plaque', 'plaque2')->first();
        $vehicule3 = Vehicule::where('plaque', 'plaque3')->first();
        $vehicule4 = Vehicule::where('plaque', 'plaque4')->first();

        $prixSaisonnier = new  \App\Models\PrixSaisonnier();
        $prixSaisonnier->prix_hiver = 100;
        $prixSaisonnier->prix_ete = 150;
        $prixSaisonnier->vehicule_id = $vehicule1->id;
        $prixSaisonnier->save();

        $prixSaisonnier = new  \App\Models\PrixSaisonnier();
        $prixSaisonnier->prix_hiver = 50;
        $prixSaisonnier->prix_ete = 100;
        $prixSaisonnier->vehicule_id = $vehicule2->id;
        $prixSaisonnier->save();

        $prixSaisonnier = new  \App\Models\PrixSaisonnier();
        $prixSaisonnier->prix_hiver = 20;
        $prixSaisonnier->prix_ete = 60;
        $prixSaisonnier->vehicule_id = $vehicule3->id;
        $prixSaisonnier->save();

        $prixSaisonnier = new  \App\Models\PrixSaisonnier();
        $prixSaisonnier->prix_hiver = 1000;
        $prixSaisonnier->prix_ete = 1500;
        $prixSaisonnier->vehicule_id = $vehicule4->id;
        $prixSaisonnier->save();
    }
}
