<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Location;
use App\Models\Vehicule;
use App\Models\User;
use App\Models\Assurance;
use Illuminate\Support\Facades\DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location')->delete();

        $user = User::where('name', 'User')->first();
        $vehicule1 = Vehicule::where('plaque', 'plaque1')->first();
        $assurance1 = Assurance::where('type_assurance', 'base')->first();

        $location = new Location();
        $location->date_location = "2020-11-24";
        $location->date_retour = "2020-11-30";
        $location->users_id = $user->id;
        $location->vehicule_id = $vehicule1->id;
        $location->assurance_id = $assurance1->id;
        $location->save();
    }
}
