<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersSeeder::class]);
        $this->call([FactureSeeder::class]);
        $this->call([EntrepotSeeder::class]);
        $this->call([CarteCreditSeeder::class]);
        $this->call([MarqueSeeder::class]);
        $this->call([ModeleSeeder::class]);
        $this->call([VehiculeSeeder::class]);
        $this->call([ImageVehiculeSeeder::class]);
        $this->call([PrixSaisonnierSeeder::class]);
        $this->call([EntretienSeeder::class]);
        $this->call([AssuranceSeeder::class]);
        $this->call([LocationSeeder::class]);
       
    }
}
