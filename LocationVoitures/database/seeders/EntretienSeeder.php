<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Entretien;
use App\Models\Vehicule;
use Illuminate\Support\Facades\DB;

class EntretienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entretien')->delete();


        $vehicule1 = Vehicule::where('plaque', 'plaque1')->first();

        $entretien = new \App\Models\Entretien();
        $entretien->date_debut = "2020-09-10";
        $entretien->date_fin = "2020-09-11";
        $entretien->vehicule_id = $vehicule1->id;
        $entretien->save();
    }
}
