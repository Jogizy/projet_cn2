<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CarteCredit;
use App\Models\User;
use Illuminate\Support\Facades\DB;


class CarteCreditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cartecredit')->delete();

        $user = User::where('name', '=', 'User')->first();

        $cartecredit = new \App\Models\CarteCredit();
        $cartecredit->utilisateur_id = $user->id;
        $cartecredit->type = 'mastercard';
        $cartecredit->titulaire = 'ezequiel';
        $cartecredit->numero = "1111111";
        $cartecredit->expiration = "un jour";
        $cartecredit->save();
    }
}
