<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ImageVehicule;
use App\Models\Vehicule;
use Illuminate\Support\Facades\DB;


class ImageVehiculeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imagevehicule')->delete();

        $vehicule1 = Vehicule::where('plaque', 'plaque1')->first();
        $vehicule2 = Vehicule::where('plaque', 'plaque2')->first();
        $vehicule3 = Vehicule::where('plaque', 'plaque3')->first();
        $vehicule4 = Vehicule::where('plaque', 'plaque4')->first();

        $imageVehicule = new ImageVehicule();
        $imageVehicule->vehicule_id = $vehicule1->id;
        $imageVehicule->image = $vehicule1->modele['nom']; //test relation tables
        $imageVehicule->save();

        $imageVehicule = new ImageVehicule();
        $imageVehicule->vehicule_id = $vehicule2->id;
        $imageVehicule->image = $vehicule2->modele['nom']; //test relation tables
        $imageVehicule->save();

        $imageVehicule = new ImageVehicule();
        $imageVehicule->vehicule_id = $vehicule3->id;
        $imageVehicule->image = $vehicule3->modele['nom']; //test relation tables
        $imageVehicule->save();

        $imageVehicule = new ImageVehicule();
        $imageVehicule->vehicule_id = $vehicule4->id;
        $imageVehicule->image = $vehicule4->modele['nom']; //test relation tables
        $imageVehicule->save();
     }
}
