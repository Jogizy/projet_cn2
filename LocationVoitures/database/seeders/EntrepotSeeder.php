<?php

namespace Database\Seeders;

use App\Models\Entrepot;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class EntrepotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entrepot')->delete();

        $entrepot = new \App\Models\Entrepot();
        $entrepot->nom = 'Entrepot 1';
        $entrepot->adresse = '1054 rue deLentrepot';
        $entrepot->capacite = 15;
        $entrepot->telephone = '(819) 477-7711';

        $entrepot->save();
    }
}
