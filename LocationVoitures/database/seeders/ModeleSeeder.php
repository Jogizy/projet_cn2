<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Modele;
use App\Models\Marque;
use Illuminate\Support\Facades\DB;

class ModeleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modele')->delete();

        $Honda = Marque::where('nom', '=', 'honda')->first();
        $Volkswagen = Marque::where('nom','=','Volkswagen')->first();
        $Mercedes = Marque::where('nom','=','Mercedes')->first();

        $m = new Modele();
        $m->nom = 'Civic';
        $m->marque()->associate($Honda);
        $m->save();

//        $m = new Modele();
//        $m->nom = 'Civic';
//        $m->marque_id = $Honda->id;
//        $m->save();
//        $m = new Modele();
//        $m->nom = 'Civic';
//        $Honda->modeles()->save($m);

        $m = new Modele();
        $m->nom = 'Odyssey';
        $m->marque()->associate($Honda);
        $m->save();

        $m = new Modele();
        $m->nom = 'Tiguan';
        $m->marque()->associate($Volkswagen);
        $m->save();

        $m = new Modele();
        $m->nom = 'AMG';
        $m->marque()->associate($Mercedes);
        $m->save();
    }
}
