<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Marque;


class MarqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marque')->delete();

        $marque = new Marque();
        $marque->nom = "Honda";
        $marque->save();

        $marque = new Marque();
        $marque->nom = "Volkswagen";
        $marque->save();

        $marque = new Marque();
        $marque->nom = "Mercedes";
        $marque->save();
    }
}
