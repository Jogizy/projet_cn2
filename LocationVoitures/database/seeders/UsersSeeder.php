<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use Illuminate\Support\Facades\DB;


class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $user = new \App\Models\User();
        $user->name = "Admin";
        $user->email = "admin@email.com";
        $user->role = 1;
        $user->password = bcrypt('adminadmin');
        $user->save();

        $user = new \App\Models\User();
        $user->name = "User";
        $user->email = "user@email.com";
        $user->role = 2;
        $user->password = bcrypt('userpassword');
        $user->save();


    }
}
