<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Facture;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class FactureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facture')->delete();

        $user = User::where('name', '=', 'User')->first();

        $facture = new \App\Models\Facture();
        $facture->utilisateur_id = $user->id;
        $facture->prix_voiture = 20;
        $facture->prix_assurance = 50;
        $facture->prix_total = 70;
        $facture->no_carte_credit = 1111111;

        $facture->save();
    }
}
