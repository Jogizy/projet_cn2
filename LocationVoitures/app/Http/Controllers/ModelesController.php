<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreModele;
use App\Models\Marque;
use App\Models\Modele;
use Illuminate\Http\Request;

class ModelesController extends Controller
{
    public function index()
    {
        $modeles = Modele::all();
        return view('modeles.index', compact('modeles'));
    }

    public function create()
    {
        $modeles = Modele::pluck('nom', 'id');
        $marques = Marque::all();
        return view('modeles.create', compact('modeles', 'marques'));
    }

    public function edit()
    {
        $modeles = Modele::all();
        return view('modeles.edit', compact('modeles'));
    }

    public function delete()
    {
        $modeles = Modele::all();
        return view('modeles.delete', compact('modeles'));
    }

    public function store(Request $request)
    {
        $m = request()->validate([
            'modele' => ['required', 'string', 'max:255'],
            'marque' => ['required']
        ]);

        $marque = Marque::where('id', '=', $m['marque'])->first();

        $modele= new Modele();
        $modele->nom = $m['modele'];
        $modele->marque()->associate($marque);
        $modele->save();
        return redirect('Modeles');
    }

    public function show($id)
    {
        $modele = Modele::findOrFail($id);
        $marque = Marque::pluck('nom', 'id');

        return view('modele.show', compact('modele', 'marque'));
    }

    public function update(Request $request)
    {
        $m = request()->validate([
            'oldModele' => ['required'],//, 'string', 'max:255'],
            'newModele' => ['required', 'string', 'max:255']
        ]);

        $modele = Modele::where('id', '=', $m['oldModele'])->first();
        $modele->nom = $m['newModele'];
        $modele->save();
        return redirect('Modeles');
    }

    public function destroy(Request $request)
    {
        $m = request()->validate([
            'modele' => ['required']//, 'string', 'max:255']
        ]);

        $modele = Modele::where('id', '=', $m['modele'])->first();
        $modele->delete();

        return redirect('Modeles');
    }
}
