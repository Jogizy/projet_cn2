<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreModele;
use App\Models\Marque;
use App\Models\Modele;

class MarquesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $marques = Marque::all();
        return view("marques.index", compact('marques'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $marques = Marque::pluck('nom', 'id');
        return view('marques.create', compact('marques'));
    }

    public function delete()
    {
        $marques = Marque::all();
        return view('marques.delete', compact('marques'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit()
    {
        $marques = Marque::all();
        return view('marques.edit', compact('marques'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $m = request()->validate([
            'marque' => 'required', 'string', 'max:255'
        ]);

        $marque = new Marque();
        $marque->nom = $m["marque"];
        $marque->save();
        return redirect('Marques');
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $m = request()->validate([
            'oldMarque' => 'required', 'numeric',
            'newMarque' => 'required', 'string', 'max:255'
        ]);

        $marque = Marque::where('id', '=', $m['oldMarque'])->first();
        $marque->nom = $m['newMarque'];
        $marque->save();
        return redirect('Marques');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $m = request()->validate([
            'marque' => 'required', 'string', 'max:255'
        ]);

        $marque = Marque::where('id', '=', $m['marque'])->first();
        $marque->delete();

        return redirect('Marques');
    }
}
