<?php

namespace App\Http\Controllers;

use App\Models\Entrepot;
use Illuminate\Http\Request;
use function PHPUnit\Framework\isEmpty;

class EntrepotsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entrepots = Entrepot::all();
        return view('entrepots.index', compact('entrepots'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $entrepots = Entrepot::all();
        return view('entrepots.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $e = request()->validate([
            'nom' => ['required', 'string', 'max:255'],
            'adresse'=> ['required', 'string', 'max:255'],
            'telephone' => ['required', 'string', 'max:255'],
            'capacite' => ['required', 'numeric', 'min:1']
        ]);

        $entrepot = new Entrepot();
        $entrepot->nom = $e['nom'];
        $entrepot->adresse = $e['adresse'];
        $entrepot->telephone = $e['telephone'];
        $entrepot->capacite = $e['capacite'];
        $entrepot->save();

        return redirect('Entrepots');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $entrepots = Entrepot::all();
        return view('entrepots.edit', compact('entrepots'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $e = request()->validate([
            'oldEntrepot' => ['required'],
            'nom' => ['required', 'string', 'max:255'],
            'adresse'=> ['required', 'string', 'max:255'],
            'telephone' => ['required', 'string', 'max:255'],
            'capacite' => ['required', 'numeric', 'min:1']
        ]);

        $entrepot = Entrepot::where('id', '=', $e['oldEntrepot'])->first();
        $entrepot->nom = $e['nom'];
        $entrepot->adresse = $e['adresse'];
        $entrepot->telephone = $e['telephone'];
        $entrepot->capacite = $e['capacite'];
        $entrepot->save();

        return redirect('Entrepots');
    }

    public function delete()
    {
        $entrepots = Entrepot::all();
        return view('entrepots.delete', compact('entrepots'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $e = request()->validate([
            'entrepot' => ['required']
        ]);

        $entrepot = Entrepot::where('id', '=', $e['entrepot'])->first();
        $entrepot->delete();

        return redirect('Entrepots');
    }
}
