<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreModele;
use App\Models\Vehicule;
use App\Models\Modele;
use App\Models\Entrepot;

class VehiculesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicules = Vehicule::all();
        return view("vehicules.index", compact('vehicules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modeles = Modele::all();
        $entrepots = Entrepot::all();
        $vehicules = Vehicule::all();

        return view('vehicules.create', compact('modeles', 'entrepots', 'vehicules'));
    }

    public function delete()
    {
        $vehicules = Vehicule::all();
        return view('vehicules.delete', compact('vehicules'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = request()->validate([
            'modele' => ['required'], 
            'entrepot' => ['required'],
            'description' => ['required', 'string', 'max:255'],
            'couleur' => ['required', 'string', 'max:255'], 
            'km' => ['required', 'numeric', 'min:0','max:999999'],
            'annee' => ['required', 'numeric', 'min:1900', 'max:2021'], 
            'date_disponibilite' => ['required', 'date'], 
            'vin' => ['required', 'numeric'],
            'plaque' => ['required', 'string', 'min:6', 'max:7']
        ]);

        $entrepot = Entrepot::where('id', '=', $v['entrepot'])->first();
        $modele = Modele::where('id', '=', $v['modele'])->first();
        $vehicule = new Vehicule();
        
        $vehicule->modele()->associate($modele);
        $vehicule->entrepot()->associate($entrepot);
        $vehicule->description = $v['description'];
        $vehicule->couleur = $v['couleur'];
        $vehicule->km = $v['km'];
        $vehicule->annee = $v['annee'];
        $vehicule->date_disponibilite = $v['date_disponibilite'];
        $vehicule->vin = $v['vin'];
        $vehicule->plaque = $v['plaque'];         
        //$vehicule->est_disponible = true;
        $timezone = date_default_timezone_get();
        if ($timezone = $vehicule->date_disponibilite) {
            $vehicule->est_disponible = true;
            echo $vehicule->est_disponible;           
        } else {
            $vehicule->est_disponible = false;   
            echo $vehicule->est_disponible;                    
        }
            
        $vehicule->save();
        return redirect('Vehicules');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $vehicules = Vehicule::all();
        $entrepots = Entrepot::all();
        $modeles = Modele::all();
        return view('vehicules.edit', compact('vehicules', 'entrepots', 'modeles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = request()->validate([
            'oldVehicule' => ["required"], 
            'newModele' => ['required'], 
            'newEntrepot' => ['required'],
            'newDescription' => ['required', 'string', 'max:255'],
            'newCouleur' => ['required', 'string', 'max:255'], 
            'newKm' => ['required', 'numeric', 'min:0','max:999999'],
            'newAnnee' => ['required', 'numeric', 'min:1900', 'max:2021'], 
            'newDateDisponibilite' => ['required', 'date'], 
            'newVin' => ['required', 'numeric'],
            'newPlaque' => ['required', 'string', 'min:6', 'max:7']
        ]);

        $entrepot = Entrepot::where('id', '=', $v['newEntrepot'])->first();
        $modele = Modele::where('id', '=', $v['newModele'])->first();
        $vehicule = Vehicule::where('id', '=', $v['oldVehicule'])->first();
        
        $vehicule->modele()->associate($modele);
        $vehicule->entrepot()->associate($entrepot);
        $vehicule->description = $v['newDescription'];
        $vehicule->couleur = $v['newCouleur'];
        $vehicule->km = $v['newKm'];
        $vehicule->annee = $v['newAnnee'];
        $vehicule->date_disponibilite = $v['newDateDisponibilite'];
        $vehicule->vin = $v['newVin'];
        $vehicule->plaque = $v['newPlaque'];    
        //Le code aurait été utile sur le projet complet mais ne l'est pas en ce moment.     
        $timezone = date_default_timezone_get();
        if ($timezone = $vehicule->date_disponibilite) {
            $vehicule->est_disponible = true;
            echo $vehicule->est_disponible;           
        } else {
            $vehicule->est_disponible = false;   
            echo $vehicule->est_disponible;                    
        }
            
        $vehicule->save();
        return redirect('Vehicules');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $v = request()->validate([
            'vehicule' => ['required', 'string', 'max:255'],
        ]);
        $vehicule = Vehicule::where('id', '=', $v['vehicule'])->first();
        $vehicule->delete();

        return redirect('Vehicules');
    }
}
