<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrixSaisonnier extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'prixsaisonnier';

    public function vehicule()
    {
        return $this->belongsTo(Vehicule::class);
    }
}
