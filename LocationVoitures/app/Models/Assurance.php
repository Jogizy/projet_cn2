<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assurance extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'assurance';

    public function modele()
    {
        return $this->hasMany(Modele::class);
    }

    public function location()
    {
        return $this->belongsToMany(Location::class);
    }
}
