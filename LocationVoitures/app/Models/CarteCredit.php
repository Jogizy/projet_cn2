<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarteCredit extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'cartecredit';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
