<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'location';

    public function vehicule()
    {
        return $this->belongsTo(Vehicule::class);
    }

    public function assurance()
    {
        return $this->belongsTo(Assurance::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
