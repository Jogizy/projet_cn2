<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicule extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'vehicule';

    function modele()
    {
        return $this->belongsTo(Modele::class);
    }

    function entrepot()
    {
        return $this->belongsTo(Entrepot::class);
    }

    function imageVehicule()
    {
        return $this->hasMany(ImageVehicule::class);
    }

    function prixSaisonnier()
    {
        return $this->hasOne(PrixSaisonnier::class);
    }

    function location()
    {
        return $this->hasMany(Location::class);
    }
}
