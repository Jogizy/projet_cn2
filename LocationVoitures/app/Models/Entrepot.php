<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entrepot extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'entrepot';

    public function vehicule()
    {
        return $this->hasMany(Vehicule::class);
    }
}
