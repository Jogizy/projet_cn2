<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageVehicule extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'imagevehicule';

    public function vehicule()
    {
        return $this->belongsTo(Vehicule::class);
    }
}
