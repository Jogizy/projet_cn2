<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marque extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'marque';

    public function modele()
    {
        return $this->hasMany(Modele::class);
    }
}
