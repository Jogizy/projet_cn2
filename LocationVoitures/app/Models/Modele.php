<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modele extends Model
{
//    use HasFactory;
    protected $guarded = array('id');
    protected $table = 'modele';

    public function marque()
    {
        return $this->belongsTo(Marque::class, 'marque_id');
    }

    public function vehicule()
    {
        return $this->hasMany(Vehicule::class);
    }

    public function assurance()
    {
        return $this->belongsTo(Assurance::class);
    }
}
