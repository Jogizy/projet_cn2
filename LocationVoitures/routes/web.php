<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/reglages', [App\Http\Controllers\ReglagesController::class, 'index'])->name('reglages');

Route::get('/Modeles', [App\Http\Controllers\ModelesController::class, 'index'])->name('modeles')->middleware('admin');
Route::post('/Modeles', [App\Http\Controllers\ModelesController::class, 'store'])->name('modeles.store')->middleware('admin');
Route::get('/Modeles/create', [App\Http\Controllers\ModelesController::class, 'create'])->name('modeles.create')->middleware('admin');
Route::get('/Modeles/edit', [App\Http\Controllers\ModelesController::class, 'edit'])->name('modeles.edit')->middleware('admin');
Route::patch('/Modeles', [App\Http\Controllers\ModelesController::class, 'update'])->name('modeles.update')->middleware('admin');
Route::get('/Modeles/delete', [App\Http\Controllers\ModelesController::class, 'delete'])->name('modeles.delete')->middleware('admin');
Route::delete('/Modeles', [App\Http\Controllers\ModelesController::class, 'destroy'])->name('modeles.destroy')->middleware('admin');
//Route::resource('Modeles', 'ModelesController')->middleware('admin'); //weird, doesnt work

Route::get('/Entrepots', [App\Http\Controllers\EntrepotsController::class, 'index'])->name('entrepots')->middleware('admin');
Route::post('/Entrepots', [App\Http\Controllers\EntrepotsController::class, 'store'])->name('entrepots.store')->middleware('admin');
Route::get('/Entrepots/create', [App\Http\Controllers\EntrepotsController::class, 'create'])->name('entrepots.create')->middleware('admin');
Route::get('/Entrepots/edit', [App\Http\Controllers\EntrepotsController::class, 'edit'])->name('entrepots.edit')->middleware('admin');
Route::patch('/Entrepots', [App\Http\Controllers\EntrepotsController::class, 'update'])->name('entrepots.update')->middleware('admin');
Route::get('/Entrepots/delete', [App\Http\Controllers\EntrepotsController::class, 'delete'])->name('entrepots.delete')->middleware('admin');
Route::delete('Entrepots', [App\Http\Controllers\EntrepotsController::class, 'destroy'])->name('entrepots.destroy')->middleware('admin');

//CRUD MARQUE --EZEQUIEL
Route::get('/Marques', [App\Http\Controllers\MarquesController::class, 'index'])->name('marques')->middleware('admin'); // INDEX
Route::post('/Marques', [App\Http\Controllers\MarquesController::class, 'store'])->name('marque.store')->middleware('admin'); //POST
Route::get('/Marques/create', [App\Http\Controllers\MarquesController::class, 'create'])->name('marque.create')->middleware('admin');//CREATE VIEW
Route::get('/Marques/edit', [App\Http\Controllers\MarquesController::class, 'edit'])->name('marque.edit')->middleware('admin'); // EDIT VIEW
Route::patch('/Marques', [App\Http\Controllers\MarquesController::class, 'update'])->name('marques.update')->middleware('admin');
Route::get('/Marques/delete', [App\Http\Controllers\MarquesController::class, 'delete'])->name('marque.delete')->middleware('admin');
Route::delete('/Marques', [App\Http\Controllers\MarquesController::class, 'destroy'])->name('marques.destroy')->middleware('admin');

//CRUD VEHICULE --EZEQUIEL
Route::get('/Vehicules', [App\Http\Controllers\VehiculesController::class, 'index'])->name('vehicules')->middleware('admin');
Route::post('/Vehicules', [App\Http\Controllers\VehiculesController::class, 'store'])->name('vehicule.store')->middleware('admin'); //POST
Route::get('/Vehicules/create', [App\Http\Controllers\VehiculesController::class, 'create'])->name('vehicule.create')->middleware('admin');//CREATE VIEW
Route::get('/Vehicules/edit', [App\Http\Controllers\VehiculesController::class, 'edit'])->name('vehicule.edit')->middleware('admin'); // EDIT VIEW
Route::patch('/Vehicules', [App\Http\Controllers\VehiculesController::class, 'update'])->name('vehicules.update')->middleware('admin');
Route::get('/Vehicules/delete', [App\Http\Controllers\VehiculesController::class, 'delete'])->name('vehicule.delete')->middleware('admin');
Route::delete('/Vehicules', [App\Http\Controllers\VehiculesController::class, 'destroy'])->name('vehicules.destroy')->middleware('admin');
