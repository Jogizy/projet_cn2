@extends('layouts.app')

@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="text-left">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h1>Réglages</h1>
                        </a>
                        <div class="container">
                            <h2>Véhicules</h2>
                            <div class="row" style="margin: 25px">
                                <div class="col-sm-9">
                                <table class="table table-hover" style="margin: 20px">
                                    <thead>
                                        @foreach($vehicules as $vehicule)
                                            <tr>
                                                <th>Modèle</th>
                                                <th>Description</th>
                                                <th>Couleur</th>
                                                <th>Année</th>
                                                <th>Milage</th>
                                            </tr>

                                            <tr>
                                                <td rowspan="2">{{ $vehicule->modele->nom}}</th>
                                                <td rowspan="3">{{ $vehicule->description}}</th>
                                                <td>{{ $vehicule->couleur }}</th>
                                                <td>{{ $vehicule->annee }}</th>
                                                <td>{{ $vehicule->km }}</th>
                                            </tr>

                                            <tr>
                                                <th>Modèle</th>
                                                <th>Description</th>
                                                <th>Couleur</th>
                                            </tr>
                                            <tr>
                                                <td><b>Disponible le: </b> {{ $vehicule->date_disponibilite }}</td>
                                                <td>{{ $vehicule->plaque }}</td>
                                                <td>{{ $vehicule->vin }}</td>
                                                <td>{{ $vehicule->entrepot->nom }}</td>
                                            </tr>
                                            <tr><td colspan="5"></td></tr>
                                        @endforeach
                                    </thead>
                                </table>
                                </div>
                                <div class="col-sm-1" style="margin: 2% 0 0 6%">
                                    <a href="{{ route('vehicule.create') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Ajouter</button>
                                    </a>
                                    <a href="{{ route('vehicule.edit') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Modifier</button>
                                    </a>
                                    <a href="{{ route('vehicule.delete') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Supprimer</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
