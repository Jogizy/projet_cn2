@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="/Vehicules" enctype="multipart/form-data" method="post">
            @csrf
            @method('PATCH')
            <section class="section-padding">
                <div class="text-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('reglages') }}" style="color: black">
                                <h1>Réglages</h1>
                            </a>
                            <div class="container">
                                <h2>Véhicules</h2>
                                <div class="row" style="margin: 25px">
                                    <div class="col-sm-6">
                                        <div>
                                            <h3>Modifier un véhicule</h3>

                                            <div class="d-flex pt-2">
                                                <label>Selectionner un véhicule:</label>
                                                <select class="form-control @error('oldVehicule') is-invalid @enderror" name="oldVehicule" >
                                                    <option></option>
                                                    @foreach($vehicules as $vehicule)
                                                        <option>{{$vehicule->id}}, {{$vehicule->modele->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex pt-2">
                                                <label>Selectionner un modèle:</label>
                                                <select class="form-control @error('newModele') is-invalid @enderror" name="newModele" >
                                                    <option></option>
                                                    @foreach($modeles as $modele)
                                                        <option>{{$modele->id}}, {{$modele->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex pt-2">
                                                <label>Selectionner un entrepot:</label>
                                                <select class="form-control @error('newEntrepot') is-invalid @enderror" name="newEntrepot" >
                                                    <option></option>
                                                    @foreach($entrepots as $entrepot)
                                                        <option>{{$entrepot->id}}, {{$entrepot->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            
                                            <div class="d-flex form-group pt-2">
                                                <label for="newDescription">Description du véhiucle:</label>
                                                <textarea rows="5" cols="20"
                                                        id="newDescription"
                                                        type="text"
                                                        name="newDescription"
                                                        class="form-control @error('newDescription') is-invalid @enderror"></textarea>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="newCouleur">Couleur du véhiucle:</label>
                                                <input  id="newCouleur"
                                                        type="text"
                                                        name="newCouleur"
                                                        class="form-control @error('newCouleur') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="newKm">Milage du véhiucle:</label>
                                                <input  id="newKm"
                                                        type="number"
                                                        name="newKm"
                                                        class="form-control @error('newKm') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="newAnnee">Année du véhiucle:</label>
                                                <input  id="newAnnee"
                                                        type="number"
                                                        name="newAnnee"
                                                        class="form-control @error('newAnnee') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="newPlaque">Plaque du véhiucle:</label>
                                                <input  id="newPlaque"
                                                        type="text"
                                                        name="newPlaque"
                                                        class="form-control @error('newPlaque') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="newVin">VIN du véhiucle:</label>
                                                <input  id="newVin"
                                                        type="number"
                                                        name="newVin"
                                                        class="form-control @error('newVin') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="newDateDisponibilite">Date de disponibilité:</label>
                                                <input  id="newDateDisponibilite"
                                                        type="date"
                                                        name="newDateDisponibilite"
                                                        class="form-control @error('newDateDisponibilite') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="row">
                                                <button type="submit" class="btn btn-primary" style="margin: 0px 370px">Sauvegarder</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

@stop
