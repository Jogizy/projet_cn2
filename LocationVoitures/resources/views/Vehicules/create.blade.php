@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="/Vehicules" enctype="multipart/form-data" method="post">
        @csrf
            <section class="section-padding">
                <div class="text-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('reglages') }}" style="color: black">
                            <h1>Réglages</h1>
                            </a>
                            <div class="container">
                                <h2>Véhicule</h2>
                                <div class="row" style="margin: 25px">
                                    <div class="col-sm-6">
                                        <div>
                                        <h3>Ajouter un véhicule</h3>
                                            <div class="d-flex">
                                                <label>Selectionner le modele:</label>
                                                <select class="form-control @error('modele') is-invalid @enderror" name="modele" >
                                                        <option></option>
                                                    @foreach($modeles as $modele)
                                                        <option>{{$modele->id}}, {{$modele->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex">
                                                <label>Selectionner un entrepot:</label>
                                                <select class="form-control @error('entrepot') is-invalid @enderror" name="entrepot" >
                                                        <option></option>
                                                    @foreach($entrepots as $entrepot)
                                                        <option>{{$entrepot->id}}, {{$entrepot->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="description">Description du vehicule:</label>
                                                    <textarea rows="5" cols="20"
                                                            id="description"
                                                            type="text"
                                                            name="description"
                                                            class="form-control @error('description') is-invalid @enderror"></textarea>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="couleur">Couleur du véhicule:</label>
                                                    <input rows="5" cols="20"
                                                            id="couleur"
                                                            type="text"
                                                            name="couleur"
                                                            class="form-control @error('couleur') is-invalid @enderror"></input>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="km">Milage du véhicule:</label>
                                                    <input rows="5" cols="20"
                                                            id="km"
                                                            type="text"
                                                            name="km"
                                                            class="form-control @error('km') is-invalid @enderror"></input>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="annee">Année du véhicule:</label>
                                                    <input rows="5" cols="20"
                                                            id="annee"
                                                            type="number"
                                                            name="annee"
                                                            class="form-control @error('annee') is-invalid @enderror"></input>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="plaque">Plaque du véhicule : </label>
                                                    <input rows="5" cols="20"
                                                            id="plaque"
                                                            type="text"
                                                            name="plaque"
                                                            class="form-control @error('date_disponibilite') is-invalid @enderror"></input>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="km">VIN du véhicule:</label>
                                                    <input rows="5" cols="20"
                                                            id="vin"
                                                            type="number"
                                                            name="vin"
                                                            class="form-control @error('vin') is-invalid @enderror"></input>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="date_disponibilite">Date de disponibilité : </label>
                                                    <input rows="5" cols="20"
                                                            id="date_disponibilite"
                                                            type="date"
                                                            name="date_disponibilite"
                                                            class="form-control @error('date_disponibilite') is-invalid @enderror"></input>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                          
                                            <div class="row pt-2">
                                                <button type="submit" class="btn btn-primary" style="margin: 0px 370px">Sauvegarder</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

@stop
