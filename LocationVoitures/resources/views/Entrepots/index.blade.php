@extends('layouts.app')

@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="text-left">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h1>Réglages</h1>
                        </a>
                        <div class="container">
                            <h2>Entrepôts</h2>
                            <div class="row" style="margin: 25px">
                                <div class="col-sm-9">
                                    <table class="table table-hover" style="margin: 20px">
                                        <thead>
                                        <tr>
                                            <th>Entrepôt</th>
                                            <th>Adresse</th>
                                            <th>Capacité</th>
                                            <th>Téléphone</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($entrepots as $entrepot)
                                            <tr>
                                                <td>{{ $entrepot->nom }}</td>
                                                <td>{{ $entrepot->adresse }}</td>
                                                <td>{{ $entrepot->capacite }}</td>
                                                <td>{{ $entrepot->telephone }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-1" style="margin: 2% 0 0 6%">
                                    <a href="{{ route('entrepots.create') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Ajouter</button>
                                    </a>
                                    <a href="{{ route('entrepots.edit') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Modifier</button>
                                    </a>
                                    <a href="{{ route('entrepots.delete') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Supprimer</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
