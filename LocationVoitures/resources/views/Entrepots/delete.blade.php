@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="/Entrepots" enctype="multipart/form-data" method="post">
            @csrf
            @method('DELETE')
            <section class="section-padding">
                <div class="text-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('reglages') }}" style="color: black">
                                <h1>Réglages</h1>
                            </a>
                            <div class="container">
                                <h2>Entrepôts</h2>
                                <div class="row" style="margin: 25px">
                                    <div class="col-sm-6">
                                        <div>
                                            <h3>Supprimer un Entrepôt</h3>

                                            <div class="d-flex pt-2">
                                                <label>Selectionner l'Entrepôt:</label>
                                                <select class="form-control @error('entrepot') is-invalid @enderror" name="entrepot" >
                                                    <option></option>
                                                    @foreach($entrepots as $entrepot)
                                                        <option value="{{ $entrepot->id }}">{{$entrepot->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="row">
                                                <button type="submit" class="btn btn-primary" style="margin-left: 80%">Supprimer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

@stop
