@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="/Entrepots" enctype="multipart/form-data" method="post">
        @csrf
            <section class="section-padding">
                <div class="text-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('reglages') }}" style="color: black">
                            <h1>Réglages</h1>
                            </a>
                            <div class="container">
                                <h2>Entrepôts</h2>
                                <div class="row" style="margin: 25px">
                                    <div class="col-sm-7">
                                        <div>
                                        <h3>Ajouter un entrepôt</h3>
                                            <div class="d-flex form-group pt-2">
                                                <label for="nom">Nom: </label>
                                                <input  id="nom"
                                                        type="text"
                                                        name="nom"
                                                        class="form-control ml-3 @error('nom') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                @enderror
                                            </div>
                                            <div class="d-flex form-group">
                                                <label for="adresse">Adresse: </label>
                                                <input  id="adresse"
                                                        type="text"
                                                        name="adresse"
                                                        class="form-control ml-3 @error('adresse') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                @enderror
                                            </div>
                                            <div class="d-flex form-group pt-2">
                                                <label for="telephone">Téléphone: </label>
                                                <input  id="telephone"
                                                        type="text"
                                                        name="telephone"
                                                        class="form-control-sm ml-3
                                                    @error('telephone') is-invalid @enderror">
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                            </div>
                                            <div class="d-flex form-group pt-2">
                                                <label for="capacite">Capacité: </label>
                                                <input  id="capacite"
                                                        type="text"
                                                        name="capacite"
                                                        class="form-control-sm ml-3
                                                    @error('capacite') is-invalid @enderror">
                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                            <div class="d-flex">
                                                <button type="submit" class="btn btn-primary" style="margin-left: 80%">Sauvegarder</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </section>
        </form>
    </div>
    </div>
@stop
