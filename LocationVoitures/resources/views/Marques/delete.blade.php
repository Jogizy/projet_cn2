@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="/Marques" enctype="multipart/form-data" method="post">
            @csrf
            @method('DELETE')
            <section class="section-padding">
                <div class="text-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('reglages') }}" style="color: black">
                                <h1>Réglages</h1>
                            </a>
                            <div class="container">
                                <h2>Marques</h2>
                                <div class="row" style="margin: 25px">
                                    <div class="col-sm-6">
                                        <div>
                                            <h3>Supprimer une marque</h3>

                                            <div class="d-flex pt-2">
                                                <label>Selectionner une marque:</label>
                                                <select class="form-control @error('marque') is-invalid @enderror" name="marque" >
                                                    <option></option>
                                                    @foreach($marques as $marque)
                                                        <option>{{$marque->id}},{{$marque->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="row">
                                                <button type="submit" class="btn btn-primary" style="margin: 0px 370px">Supprimer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

@stop
