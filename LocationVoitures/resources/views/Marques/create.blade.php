@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="/Marques" enctype="multipart/form-data" method="post">
            @csrf
            <section class="section-padding">
                <div class="text-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('reglages') }}" style="color: black">
                                <h1>Réglages</h1>
                            </a>
                            <div class="container">    
                                <h2>Marques</h2>   

                                <div class="row" style="margin: 25px">
                                    <div class="col-sm-6">
                                        <h3>Ajouter une marque</h3>
                                        <div class="d-flex form-group pt-2">                        
                                            <label for="marque" class="col-md-4 col-form-label text-md-right">Marque :</label>
                                            <input  id="marque"
                                                    type="text"
                                                    name="marque"
                                                    class="form-control @error('marque') is-invalid @enderror"
                                                    >
                                        @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                <div class="row pt-2">
                                    <button type="submit" class="btn btn-primary" style="margin: 20px 368px">Sauvegarder</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

@stop
