@extends('layouts.app')

@section('content')

    <div class="container">
    <form action="/Marques" enctype="multipart/form-data" method="post">
        @csrf
        @method('PATCH')
        <section class="section-padding">
            <div class="text-left">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h1>Réglages</h1>
                        </a>
                        <div class="container">
                            <h2>Marques</h2>                           
                            <div class="row" style="margin: 25px">
                                <div class="col-sm-6">
                                    <h3>Modifier une marque</h3>

                                    <div class="d-flex pt-2">
                                        <label>Selectionner une marque:</label>
                                        <select class="form-control @error('oldMarque') is-invalid @enderror" name="oldMarque" >
                                                <option></option>
                                            @foreach($marques as $marque)
                                                <option>{{$marque->id}}, {{$marque->nom}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                        <div class="d-flex form-group pt-2">
                                                <label for="newMarque">Nouveau nom de la marque:</label>
                                                <input  id="newMarque"
                                                        type="text"
                                                        name="newMarque"
                                                        class="form-control @error('newMarque') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                        </div>

                                        </div class="row">
                                            <button type="submit" class="btn-primary" style="margin: 20px 368px">Sauvegarder</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
