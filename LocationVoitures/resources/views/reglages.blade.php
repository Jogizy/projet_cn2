@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section-padding">
            <div class="text-left">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1>Réglages</h1>
                        <div class="container pt-2">
                            <a href="{{ route('entrepots') }}" style="color: #1a202c">
                                <h3>Entrepôts</h3>
                            </a>
                            <a href="{{ route('marques') }}" style="color: #1a202c">
                                <h3>Marques</h3>
                            </a>
                            <a href="{{ route('modeles') }}" style="color: #1a202c">
                                <h3>Modèles</h3>
                            </a>
                            <a href="{{ route('vehicules') }}" style="color: #1a202c">
                                <h3>Vehicules</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
