@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="/Modeles" enctype="multipart/form-data" method="post">
            @csrf
            @method('PATCH')
            <section class="section-padding">
                <div class="text-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('reglages') }}" style="color: black">
                                <h1>Réglages</h1>
                            </a>
                            <div class="container">
                                <h2>Modèles</h2>
                                <div class="row" style="margin: 25px">
                                    <div class="col-sm-6">
                                        <div>
                                            <h3>Modifier un modèle</h3>

                                            <div class="d-flex pt-2">
                                                <label>Selectionner le modèle:</label>
                                                <select id="modeleId" class="form-control @error('oldModele') is-invalid @enderror" name="oldModele" >
                                                    <option></option>
                                                    @foreach($modeles as $modele)
                                                        <option value="{{ $modele->id }}">{{ $modele->nom }}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="d-flex form-group pt-2">
                                                <label for="newModele">Nouveau nom du modèle:</label>
                                                <input  id="newModele"
                                                        type="text"
                                                        name="newModele"
                                                        class="form-control @error('newModele') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="row">
                                                <button type="submit" class="btn btn-primary" style="margin: 0px 370px">Sauvegarder</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

@stop
