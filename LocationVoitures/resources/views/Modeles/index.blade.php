@extends('layouts.app')

@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="text-left">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h1>Réglages</h1>
                        </a>
                        <div class="container">
                            <h2>Modèles</h2>
                            <div class="row" style="margin: 25px">
                                <div class="col-sm-6">
                                    <table class="table table-hover" style="margin: 20px">
                                        <thead>
                                        <tr>
                                            <th>Modèle</th>
                                            <th>Marque</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($modeles as $modele)
                                            <tr>
                                                <td>{{ $modele->nom }}</td>
                                                <td>{{ $modele->marque->nom}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-1" style="margin: 2% 0 0 6%">
                                    <a href="{{ route('modeles.create') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Ajouter</button>
                                    </a>
                                    <a href="{{ route('modeles.edit') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Modifier</button>
                                    </a>
                                    <a href="{{ route('modeles.delete') }}">
                                        <button type="button" class="btn-primary m-2" style="width: 100px">Supprimer</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
