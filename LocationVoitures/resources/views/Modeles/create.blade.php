@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="/Modeles" enctype="multipart/form-data" method="post">
        @csrf
            <section class="section-padding">
                <div class="text-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ route('reglages') }}" style="color: black">
                            <h1>Réglages</h1>
                            </a>
                            <div class="container">
                                <h2>Modèles</h2>
                                <div class="row" style="margin: 25px">
                                    <div class="col-sm-6">
                                        <div>
                                        <h3>Ajouter un modèle</h3>
                                            <div class="d-flex form-group pt-2">
                                                <label for="modele">Nom du modèle:</label>
                                                <input  id="modele"
                                                        type="text"
                                                        name="modele"
                                                        class="form-control @error('modele') is-invalid @enderror">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="d-flex">
                                                <label>Selectionner la marque:</label>
                                                <select class="form-control @error('marque') is-invalid @enderror" name="marque" >
                                                        <option></option>
                                                    @foreach($marques as $marque)
                                                        <option value="{{ $marque->id }}">{{$marque->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="row pt-2">
                                                <button type="submit" class="btn btn-primary" style="margin: 0px 370px">Sauvegarder</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

@stop
